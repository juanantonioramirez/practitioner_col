var express = require('express');
var bodyParser = require('body-parser');
var requestJSON = require('request-json');
var app = express();
var port = process.env.PORT || 3000;
var userFile = require('./users.json');
var URLbase = "/colapi/v3/";

var baseMLabURL = 'https://api.mlab.com/api/1/databases/colapidb_yb/collections/';
var apiKeyMLab = 'apiKey=fL-u63JFoksG1mivN-NbfBkLC2EfvVaN';

app.listen(port);
app.use(bodyParser.json());
console.log("Colapi escuchando en puerto" + port + "..." );

//LOGOUT mLab
app.post(URLbase + 'logout',
  function(req, res) {
    console.log("LOGINmLab/colapi/V3");
    var constasenaIng = req.body.password;
    var correo = req.body.email;
    var usuario = req.body.id;
    var clienteMlab = requestJSON.createClient(baseMLabURL);
    var queryString1 = 'q={"id":';
    var queryString2 ='}&';
    var comilla = "''";

    clienteMlab.get('user?' + queryString1 + usuario + queryString2 + apiKeyMLab,
     function(errMLab, respuestaMLab, bodymLab) {
      console.log(clienteMlab);
      console.log(bodymLab);
      var consulta = {};
      consulta = bodymLab[0];
      console.log(consulta[0]);
      console.log(consulta);
      var contrasenaCon = consulta.password;

      var nuevoDes = {
        "logged": true
                     }
      console.log(nuevoDes);
      var cambio = '{"$unset":' + JSON.stringify(nuevoDes) + '}';
      console.log(cambio);

      clienteMlab.put('user?'+ queryString1 + usuario + queryString2 + apiKeyMLab, JSON.parse(cambio),
          function(errMLab2, respuestaMLab2, bodymLab2) {
          console.log('Error2 : ' + errMLab2);
          console.log('Respuesta MLab2 : ' + respuestaMLab2);
          console.log('Body2 : ' + bodymLab2);
          res.send({"msg" : "Logout correcto.", "idUsuario" : consulta.email});
        });
     });
  });


//LOGIN mLab
app.post(URLbase + 'login',
  function(req, res) {
    console.log("LOGINmLab/colapi/V3");
    var constasenaIng = req.body.password;
    var correo = req.body.email;
    console.log(req.body.email);
    console.log(req.body.password);
    var queryString1 = 'q={"email":';
    var queryString2 ='}&';
    var comilla = "'";
    var clienteMlab = requestJSON.createClient(baseMLabURL);

    clienteMlab.get('user?' + queryString1 + comilla + correo +comilla + queryString2 + apiKeyMLab,
      function(errMLab, respuestaMLab, bodymLab) {
      console.log("clienteMlab:    ",clienteMlab.get);
      console.log("bodymLab",      bodymLab);
      console.log("errMLab", errMLab)
      if(bodymLab.length === 0) {
        res.send({"msg" : "Error", "falla":true});
      } else {

      var contrasenaCon = bodymLab[0].Password;
      console.log(bodymLab[0].Password);

      console.log(contrasenaCon);
      console.log(constasenaIng);

      if(constasenaIng == contrasenaCon) {
        bodymLab[0].logged = true;
        console.log("Login correcto");

        var cambio = '{"$set":' + JSON.stringify(bodymLab[0]) + '}';
        console.log(cambio);
        console.log(JSON.parse(cambio));

        clienteMlab.put('user?'+ queryString1 + comilla + correo +comilla + queryString2 + apiKeyMLab, JSON.parse(cambio),
          function(errMLab2, respuestaMLab2, bodymLab2) {
          console.log('Error2 : ' + errMLab2);
          console.log('Respuesta MLab2 : ' + respuestaMLab2);
          console.log('Body2 : ' + bodymLab2);
          var usuarioConsulta = bodymLab[0].id;
          var logeado = bodymLab[0].logged;
          var nombre = bodymLab[0].first_name;
          var apellido = bodymLab[0].last_name;
          res.send({"msg" : "loginok", "idUsuario" : usuarioConsulta,"logeado":logeado, "nombre":nombre, "apellido":apellido});
        });

       } else {
        res.send({"msg" : "Contraseña incorrecta.", "idUsuario" : bodymLab.email, "falla" : true});
       }
     }
    });
  });


  //GET con id MLab
  app.get(URLbase + 'users/:id',
   function (req, res) {
     console.log("GET /colapi/v3/users/:id");
     console.log(req.params.id);
     var id = req.params.id;
     var queryString = 'q={"id":'+ id +'}&f={"_id":0}&';
     var httpClient = requestJSON.createClient(baseMLabURL);
     httpClient.get('user?' + queryString + apiKeyMLab,
       function(errMLab, respuestaMLab, bodymLab) {
       console.log('Error : ' + errMLab);
       var respuestaBody = {};
       respuestaBody = !errMLab ? bodymLab : {"msg" : "Error al recuperar users de mLab"};

       res.send(respuestaBody[0])  ;
     });

  });

//POST mLab, crear un cliente
app.post(URLbase + 'users',
  function(req, res) {
    console.log("PUT/colapi/V3");
    var clienteMlab = requestJSON.createClient(baseMLabURL);
    var queryString = 'f={"_id":0, "first_name":0, "last_name":0, "email":0, "password":0}&s={"id":-1}&l=1&';
   //
    clienteMlab.get('user?' + queryString + apiKeyMLab,
      function(errMLab, respuestaMLab, bodymLab) {
      console.log(bodymLab);
      var idRecibe = {};
      idRecibe = bodymLab[0];
      var numero = idRecibe.id;
      var numero2 = parseInt(numero) + 1;
      console.log(idRecibe.id);
      console.log(numero2);
      var nuevoUsuario = {
         "id" : numero2,
         "first_name" : req.body.first_name,
         "last_name" : req.body.last_name,
         "email" : req.body.email,
         "password" : req.body.password
      };
    //
    clienteMlab.post('user?'+ apiKeyMLab, nuevoUsuario,
      function(errMLab, respuestaMLab, bodymLab) {
      console.log('Error : ' + errMLab);
      console.log('Respuesta MLab : ' + respuestaMLab);
      console.log('Body : ' + bodymLab);
      res.send(nuevoUsuario);
   });
  });
});


// CUENTA

//GET con id MLab
app.get(URLbase + 'users/:id/accounts',
 function (req, res) {
   console.log("GET /colapi/v3/accounts/:id");
   console.log(req.params.id);
   var id = req.params.id;
   var queryString = 'q={"user_id":'+ id +'}&f={"_id":0}&';
   var httpClient = requestJSON.createClient(baseMLabURL);
   httpClient.get('account?' + queryString + apiKeyMLab,
     function(errMLab, respuestaMLab, bodymLab) {
     console.log('Error : ' + errMLab);
     var respuestaBody = {};
     respuestaBody = !errMLab ? bodymLab : {"msg" : "Error al recuperar cuenta de mLab"};
     console.log(respuestaBody)
     res.send(respuestaBody)  ;
   });

});

// MOVIMIENTOS

//GET con id MLab
app.get(URLbase + 'movements/:iban',
 function (req, res) {
   console.log("GET /colapi/v3/accounts/movements");
   var cuenta = req.params.iban;
   console.log(cuenta);
   var queryString1 = 'q={"IBAN":"';
   var queryString2 ='"}&';
   var comilla = "'";
   var clienteMlab = requestJSON.createClient(baseMLabURL);
   console.log('movement?' + queryString1  + cuenta + queryString2 + apiKeyMLab);

   clienteMlab.get('movement?' + queryString1 + cuenta + queryString2 + apiKeyMLab,
     function(errMLab, respuestaMLab, bodymLab) {
     console.log('Error : ' + errMLab);
     var respuestaBody = {};
     respuestaBody = !errMLab ? bodymLab : {"msg" : "Error al recuperar movimientos de mLab", "falla":true};
     console.log(respuestaBody)
     res.send(respuestaBody)  ;
   });

});
